package pl.edu.pwsztar.service.chessServiceImpl;

import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.service.ChessService;


@Service
public class ChessServiceImpl implements ChessService {
    @Override
    public boolean isMoveCorrect(FigureMoveDto figureMoveDto) {
        if(FigureType.BISHOP == figureMoveDto.getType()){
            return checkBishopMove(figureMoveDto);
        }else{
            return false;
        }
    }

    private boolean checkBishopMove(FigureMoveDto figureMoveDto) {
        String[] startPos = figureMoveDto.getStart().split("_");
        String[] destinationPos = figureMoveDto.getDestination().split("_");

        int startX = startPos[0].charAt(0);
        int destX = destinationPos[0].charAt(0);
        int startY = Integer.parseInt(startPos[1]);
        int destY = Integer.parseInt(destinationPos[1]);

        return Math.abs(startX - destX) == Math.abs(startY - destY);
    }
}
